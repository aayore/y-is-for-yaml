# Working with Strings

---

So far, we've seen relatively small chunks of data.  But there are many cases where you might need to add a larger block of text (or code) inside a YAML document.

There are two ways to manage large strings like this, and they have a pretty simple difference:

`|` - Literal Block preserves line breaks

`>` - Folded Block replaces line breaks with spaces (*and more)

The choice of characters for this seems intentional.  The rigid, vertical pipe will retain it's height.  The hinged greater-than sign will fold and collapse all the line breaks.

Both of these options strip the first level of intendation while keeping the rest.

```yaml
literal_block: |
    This is a literal block.
    Line breaks will be preserved.
    # will not be treated as a comment.

literal_block_with_more_indentation: |
        This is a literal block.
        Line breaks will be preserved.
        When parsed, this will have
        the same indentation as the
        block above.

folded block: >
    This is a folded block. 
    Line breaks will disappear in the resulting text.
    # will not be treated as a comment here, either.
```

Notes about folded blocks:
- Blank lines will be converted to line breaks
- Additionally-indented lines will be treated like literal blocks
- All folded blocks get a line break at the end.

Adding to our data...

```yaml
PetData:
    Dog: &dog
        Species: Dog
        SpeciesDescription: The dog or domestic dog (Canis familiaris or Canis lupus familiaris) is a domesticated descendant of the wolf, and is characterized by an upturning tail.
        Legs: 4
        Fur: true
    Snake: &snake
        Species: Snake
        SpeciesDescription: Snakes are elongated, limbless, carnivorous reptiles of the suborder Serpentes.
        Legs: 0
        Fur: false

Pets:
    -   Name: Fido
        <<: *dog
        Age: 4
        YOB: "2018"
        Male: true
        WeightKg: 36.23
        VetVisits:
            "2022-06-18":
                - Bordetella
            "2022-03-09":
                - Rabies
                - Distemper
        Description: >  # Will lose line breaks
            Fido is a great pup who loves long walks on the beach,
            quiet nights at home, and cuddling by the fire.
                - Not really a barker
                Pretty silly
                - Loves cats
            
            Guaranteed to run in to the screen door.
            Loves riding in the car.
    -   Name: Barkley
        <<: *dog
        Species: Muppet
        SpeciesDescription: A Muppet is a member of The Muppets - an American ensemble cast of puppet characters known for an absurdist, burlesque, and self-referential style of variety-sketch comedy.
        Age: 43
        YOB: "1978"
        Male: true
        WeightKg: 12.7
        Description: |  # Will keep line breaks
            Barkley is a full-bodied Muppet dog who appears on 
            Sesame Street. Originally called Woof Woof when he 
            made his debut in Episode 1177, he was named Barkley 
            by the residents of Sesame Street in the premiere 
            episode of the show's 10th season.
```

Running this command:

```shell
cat doggy.yaml | yq '.Pets[] | .Description'
```

Results in this output:

```
Fido is a great pup who loves long walks on the beach, quiet nights at home, and cuddling by the fire.
    - Not really a barker
       Pretty silly
    - Loves cats

Guaranteed to run in to the screen door. Loves riding in the car.

Barkley is a full-bodied Muppet dog who appears on
Sesame Street. Originally called Woof Woof when he
made his debut in Episode 1177, he was named Barkley
by the residents of Sesame Street in the premiere
episode of the show's 10th season.
```

---

## Lab

You just learned that there's an 80-character maximum line width standard for the Engineering team.  To adopt this standard in your YAML file, you're going to need folded blocks.  Additionally, the Social Media team loves it so much they want to keep all their hot promo quotes in it!

1. Update your game descriptions to use folded blocks
1. On your closest upcoming release, add a `Reviews` literal block.  These should look like:
    ```
    "Best game I've played in the last hour..."
                            --Meh Gamer
    
    "Writing a haiku
     about how much this game rocks
     it won't leave you sad"
                        --Senpai Senryu
    ```