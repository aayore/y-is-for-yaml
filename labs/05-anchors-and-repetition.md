# Anchors and Aliases

---

One of the most powerful features in YAML is its ability to replicate data.

An `anchor` creates a reference to a data object.

An `alias` imports the data object from an anchor.

These allow us to create default data objects and reduce duplication in our YAML file.

---

## Referencing a Single Value

Let's add some generic data about our pets.  In the case of `Fido`, we're able to reference the `dog_species_desc` anchor.  Because `Barkley` is a Muppet, we don't want to reference the anchor.

```yaml
PetData:
    Dog:
        SpeciesDescription: &dog_species_desc The dog or domestic dog (Canis familiaris or Canis lupus familiaris) is a domesticated descendant of the wolf, and is characterized by an upturning tail.
    Snake:
        SpeciesDescription: &snake_species_desc Snakes are elongated, limbless, carnivorous reptiles of the suborder Serpentes.

Pets:
    -   Name: Fido
        Species: Dog
        SpeciesDescription: *dog_species_desc
        Age: 4
        YOB: "2018"
        Male: true
        WeightKg: 36.23
        VetVisits:
            "2022-06-18":
                - Bordetella
            "2022-03-09":
                - Rabies
                - Distemper
    -   Name: Barkley
        Species: Muppet
        SpeciesDescription: A Muppet is a member of The Muppets - an American ensemble cast of puppet characters known for an absurdist, burlesque, and self-referential style of variety-sketch comedy.
        Age: 43
        YOB: "1978"
        Male: true
        WeightKg: 12.7

```

**Bash**

`yq` doesn't necessarily behave as expected with anchors and aliases.

```shell
cat doggy.yaml | yq '.Pets[].SpeciesDescription'
```

Results in:

```yaml
*dog_species_desc
A Muppet is a member of The Muppets - an American ensemble cast of puppet characters known for an absurdist, burlesque, and self-referential style of variety-sketch comedy.
```

That kinda makes sense, but we don't want to see the alias.  We want to see the data at the anchor.  To achieve this, we need to use the `yq` function `explode()`:

```shell
cat doggy.yaml | yq 'explode(.) | .Pets[].SpeciesDescription'
```

Results in:

```yaml
The dog or domestic dog (Canis familiaris or Canis lupus familiaris) is a domesticated descendant of the wolf, and is characterized by an upturning tail.
A Muppet is a member of The Muppets - an American ensemble cast of puppet characters known for an absurdist, burlesque, and self-referential style of variety-sketch comedy.
```

**Python**

Python handles anchors and aliases in a more seamless manner.  

```python
for dog in data['Pets']:
     print(dog['SpeciesDescription'])
```

We can see that the alias is automatically populated with data from the anchor:

```yaml
The dog or domestic dog (Canis familiaris or Canis lupus familiaris) is a domesticated descendant of the wolf, and is characterized by an upturning tail.
A Muppet is a member of The Muppets - an American ensemble cast of puppet characters known for an absurdist, burlesque, and self-referential style of variety-sketch comedy.
```

---

## Referencing Data Objects

Since we're probably going to end up with a lot of dogs in here, we want to reduce the amount of data we have to replicate.  Most dogs will have 4 legs.  Most dogs will have fur.

We're going to add this data to `.PetData.Dog` (and remove the SpeciesDescription anchors).  We're also going to delete the `.Species` key for Fido, since we're going to be adding this with our anchor/alias combination.

When importing a data object instead of a single value, we use a `<<: *alias` syntax.  This basically means "insert the whole data object here."

```yaml
PetData:
    Dog: &dog
        Species: Dog
        SpeciesDescription: The dog or domestic dog (Canis familiaris or Canis lupus familiaris) is a domesticated descendant of the wolf, and is characterized by an upturning tail.
        Legs: 4
        Fur: true
    Snake: &snake
        Species: Snake
        SpeciesDescription: Snakes are elongated, limbless, carnivorous reptiles of the suborder Serpentes.
        Legs: 0
        Fur: false

Pets:
    -   Name: Fido
        <<: *dog
        Age: 4
        YOB: "2018"
        Male: true
        WeightKg: 36.23
        VetVisits:
            "2022-06-18":
                - Bordetella
            "2022-03-09":
                - Rabies
                - Distemper
    -   Name: Barkley
        Species: Muppet
        SpeciesDescription: A Muppet is a member of The Muppets - an American ensemble cast of puppet characters known for an absurdist, burlesque, and self-referential style of variety-sketch comedy.
        Age: 43
        YOB: "1978"
        Male: true
        WeightKg: 12.7

```

And this works like we expect it to:

```shell
cat doggy.yaml | yq 'explode(.) | .Pets[] | select(.Name == "Fido")'
```

We can see the `.PetData.Dog` data for Fido.

```yaml
Name: Fido
Species: Dog
SpeciesDescription: The dog or domestic dog (Canis familiaris or Canis lupus familiaris) is a domesticated descendant of the wolf, and is characterized by an upturning tail.
Legs: 4
Fur: true
Age: 4
YOB: "2018"
Male: true
WeightKg: 36.23
VetVisits:
  "2022-06-18":
    - Bordetella
  "2022-03-09":
    - Rabies
    - Distemper
```

---

## Overrides

We would also like to indicate that Barkley has fur and 4 legs.  He's mostly dog.  But we want to keep his Species and SpeciesDescription, because he's not *all* dog.  We can do this with overrides.  They're really easy.

```yaml
PetData:
    Dog: &dog
        Species: Dog
        SpeciesDescription: The dog or domestic dog (Canis familiaris or Canis lupus familiaris) is a domesticated descendant of the wolf, and is characterized by an upturning tail.
        Legs: 4
        Fur: true
    Snake: &snake
        Species: Snake
        SpeciesDescription: Snakes are elongated, limbless, carnivorous reptiles of the suborder Serpentes.
        Legs: 0
        Fur: false

Pets:
    -   Name: Fido
        <<: *dog
        Age: 4
        YOB: "2018"
        Male: true
        WeightKg: 36.23
        VetVisits:
            "2022-06-18":
                - Bordetella
            "2022-03-09":
                - Rabies
                - Distemper
    -   Name: Barkley
        <<: *dog          # Even though we're importing .Species here
        Species: Muppet   # We're overriding it locally
        SpeciesDescription: A Muppet is a member of The Muppets - an American ensemble cast of puppet characters known for an absurdist, burlesque, and self-referential style of variety-sketch comedy.
        Age: 43
        YOB: "1978"
        Male: true
        WeightKg: 12.7

```

Now we can see that Barkley has the `.PetData.Dog` data (4 legs and fur), with the exception of the `.Species` and `.SpeciesDescription` that we've overridden.

```shell
cat doggy.yaml | yq 'explode(.) | .Pets[] | select(.Name == "Barkley")'
```

```yaml
Name: Barkley
Legs: 4
Fur: true
Species: Muppet
SpeciesDescription: A Muppet is a member of The Muppets - an American ensemble cast of puppet characters known for an absurdist, burlesque, and self-referential style of variety-sketch comedy.
Age: 43
YOB: "1978"
Male: true
WeightKg: 12.7
```

Different parsers might handle this differently (so be careful), but for both Python and `yq`, it doesn't matter where you merge the alias.

```yaml
-   Name: Barkley
    <<: *dog
    Species: Muppet
```

Will be precessed the same as:

```yaml
-   Name: Barkley
    Species: Muppet
    <<: *dog
```

Rather than think of things from a "line-by-line processing" perspective, remember that "local" data takes precedence over "remote" data.

---

## Lab

Like most game studios, your company has a pretty specific niche.  Instead of repeating this information for every game in your catalog, make a data object representing some sane defaults:

```yaml
GameData:
    Default: &game_defaults
        Genre: ???
        Local Co-op: ???
        Online Play: ???
        Number of local players supported: ???
        List of release platforms:
            - ???
            - ???
            - ???
```

1. Add this (with `???` replaced, obvs) to the top of your `games.yaml` file.
1. Update each of your games to use this as a default.
1. Make sure you override values where necessary!
