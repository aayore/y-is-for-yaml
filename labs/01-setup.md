# Intro and Set Up

---

## Class Conventions

1. Observe
    - The instructor will use a dogs/pets example
1. Then do
    - You will work with a video game example
    - You should be able to copy/paste the dogs/pets example, but you'll have to modify it for your video game work

---

## Data Structures

Natively supported data structures in YAML:

- Block sequences

    - List or Array

        ```yaml
        - 1
        - 2
        - 3
        - 4
        - 5
        
        - a
        - b
        - c

        - 17
        - fluff
        - lyrics
        - 4
        ```

        Other languages use a variation on this common syntax:

        ```yaml
        [ 1, 2, 3, 4, 5 ]

        [ "a", "b", "c" ]

        [ 17, "fluff", "lyrics", 4 ]
        ```
    
        (This syntax is also valid YAML, BTW.)

- Mappings

    - Dictionary or Hash

        ```yaml
        dog: bark
        cat: meow

        USA: 1776
        Colorado: 1876
        Denver: 1858
        ```

        Again, other languages tend to use something that looks more like this for similar data:

        ```yaml
        { "dog": "bark", "cat": "meow" }

        { "USA": 1776, "Colorado": 1876, "Denver": 1858 }
        ```

        (Also valid YAML.)

- Comments (a.k.a. not allowed in JSON)

    - Similar to many programming languages

        ```yaml
        # This is a comment
        ```


It may be possible to represent other data structures using the above.  But it also may not be pretty.  Other common data structures - Tree, Linked List, Queue, Stack, etc. - can generally be represented by a combination of Mappings or Block Sequences.

<!--
A tree like this:

```
      1
    /   \
   /     \
  2       3
 / \     / \
|   |   |   |
4   5   6   7
```

Might look something like this in YAML:

```yaml
tree:
    node: 1
    - node: 2
        - node: 4
            - null
            - null
        - node: 5
            - null
            - null
    - node: 3
        - node: 6
            - null
            - null
        - node: 7
            - null
            - null
```
-->

---

## Syntax Note: Documents

In YAML, it is properly proper to use `---` to start of a document and `...` to end a document.  You will rarely see this.

Let's look at this file:

```yaml
dog: bark
cat: meow

USA: 1776
Colorado: 1876
Denver: 1858
```

All YAML parsers (that I know of) will create a single Mapping - not two - from that data.  To explicitly demand that your parser create two Mappings, you must use document separators:

```yaml
---
dog: bark
cat: meow
...

---
USA: 1776
Colorado: 1876
Denver: 1858
...
```

While this syntax is technically correct, it varies wildly in implementation in the wild.  Here are the two most common variations:

- The initial document start (`---`) is not required
    - Every subsequent document start must succeed a newline (`\n`)
- Document end (`...`) are not required

Thus, the following would frequently be considered acceptable syntax:

```yaml
dog: bark
cat: meow

---
USA: 1776
Colorado: 1876
Denver: 1858
```

Additionally, this syntax is considered acceptable for the purposes of this class.

---

## Clone the Starter Repls to Your Account

1. Log in to [Replit](https://replit.com)
1. Select `My Repls` from the left-side menu
1. Create a folder for `Y is for YAML`
1. Go to https://replit.com/@aayore
1. For each `YAML-Bash` and `YAML-Python`:
    1. Click the Repl
    1. Click the `Fork Repl` button
    1. Click the `Run` button and make sure it works
    1. Go back to your dashboard
    1. Move the Repl to your `Y is for YAML` folder

---

![Left Side Menu](/labs/images/left-side-menu.png)

![New Folder Button](/labs/images/new-folder-button.png)

![New Folder Modal](/labs/images/new-folder-modal.png)

![Repls to Fork](/labs/images/repls-to-fork.png)

![Forking](/labs/images/how-to-fork.png)

![Left Side Menu](/labs/images/left-side-menu.png)

![Move a Repl 1](/labs/images/moving-a-repl-1.png)

![Move a Repl 2](/labs/images/moving-a-repl-2.png)

---

