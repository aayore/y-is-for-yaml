# Writing Basic YAML

---

Data Types, Data Structures, and Indentation

---

## Before We Begin

**Did your Repls work?**

Make sure you navigate into each Repl and click the `Run` button.  You should see output based on the `demodata.yaml` file.

---

## Data Types

"Data Type" is the label for an atomic unit of data in YAML.  This include:
- String
- Number
- Boolean
- Float
- Date
- and more (scientific notation, datetime, etc.)

*Data Type examples:*

```yaml
# String
Fido

# Number
4

# String
"2018"

# Boolean
true

# Float
36.23
```

---

## Data Structures

"Data Structures" refers to the arrangement of >1 data types.  These usually have a hierarchy and relation to each other.

*Data Structure examples:*

```yaml
# Mapping
Fido:
    Species: Dog
    Age: 4
    YOB: "2018"
    Male: true
    WeightKg: 36.23

# Identical
Fido: { Species: Dog, Age: 4, YOB: "2018", Male: true, WeightKg: 36.23 }


# Block Sequence
Vaccinations:
    - Rabies
    - Distemper
    - Bordetella

# Identical:
Vaccinations: [ Rabies, Distemper, Bordetella ]
```

Data types and data structures can be combined in many ways:

```yaml
# These can be combined and nested:
Fido:
    Species: Dog
    Age: 4
    YOB: "2018"
    Male: true
    WeightKg: 36.23
    VetVisits:
        2022-06-18:
            - Bordetella
        2022-03-09:
            - Rabies
            - Distemper
```

For comparison, the above YAML document would look like this in JSON.
NOTE: "Date" is not a valid type in JSON.  Here it is shown converted to a string.

*Pretty example:*
```json
{
    "Fido": {
        "Species": "Dog",
        "Age": 4,
        "YOB": "2018",
        "Male": true,
        "WeightKg": 36.23,
        "VetVisits": [
            "2022-06-18": [
                "Bordetella"
            ],
            "2022-03-09": [
                "Rabies",
                "Distemper
            ]
        ]
    }
}
```

*Same thing.  Still valid JSON.  But not pretty.*

```json
{"Fido":{"Species":"Dog","Age":4,"YOB":"2018","Male":true,"WeightKg":36.23,"VetVisits":["2022-06-18":["Bordetella"],"2022-03-09":["Rabies","Distemper]]}}
```

---

## Comments!

Some people like JSON because of its clarity.  But even those people tend to lament its lack of support for comments.  On the other hand, YAML makes it simple.  There is no special syntax for multi-line comments, which means anything on any line after a `#` will be a comment.  (You've already seen some comments above.)

```yaml
# This is my YAML file!
Fido:  # <-- I love Fido
    Species: Dog
    Age: 4 # (I got him as a puppy)
    YOB: "2018"
    Male: true
    WeightKg: 36.23
    Description1: Fido's favorite charachter is #: the octothorpe!
    Description2: "Fido's favorite characters is #: the octothorpe!"
```

Most YAML strings do not require quotes.  But a `#` can cause problems.  To get around this, use quotes.

---

## Indentation!  Indentation!  Indentation!

![Indentation](/labs/images/ballmer.jpeg)

Indentation is critical in YAML.  Usually.  This is nice (usually) because - unlike the JSON example above - YAML cannot (usually) be collapsed into a hard-to-read format.

*An indentation ambiguity:*
```yaml
# This example:
Dogs:
    - Fido
    - Barkley

# Is the same as this:
Dogs:
- Fido
- Barkley
```

I recommend and prefer the first example.  And I believe it aligns with the overall YAML ethos.  

Let's see what it looks like when we add a Mapping on the next line:

```yaml
Dogs:
- Fido
- Barkley
Caretaker: Big Bird
```

This would result in two separate Mappings, but it's difficult to read.  You may infer that the `Caretaker` Mapping should be part of the `Dogs` object.

```yaml
{
    Dogs: [ Fido, Barkley ],
    Caretaker: Big Bird
}
```

This example is much more readable:

```yaml
Dogs:
    - Fido
    - Barkley
Caretaker: Big Bird
```

---

## JSON vs. YAML

A quick side note:

YAML is a superset of JSON.  Therefore JSON is a subset of YAML.

**All JSON is YAML.  Not all YAML is JSON.***

It is valid to write YAML like this:

```yaml
{ "Dogs": [ "Fido", "Barkley" ], "Caretaker": "Big Bird" }
```

---

# Lab

You work in the marketing department for a major video game publisher.  The Engineering team is usually a bit snide when providing information, so you decide to impress them with your technical acumen.  You want to represent the details for this year's upcoming games in YAML format.

1. Invent at least three (3) games.
1. Each game should have:
    - Title
    - Release Date
    - Genre
    - Local Co-op (boolean)
    - Online Play (boolean)
    - Number of local players supported (1-4)
    - List of release platforms (e.g. Xbox, Playstation, Switch, PC, iPhone, Android, etc.)
    - Description (1-2 sentences)
1. Put this information in a `games.yaml` file in your `YAML-Python` Repl.
1. Update the file names in:
    - `main.sh` in the `YAML-Bash` repl: `FILE_TO_PARSE`
    - `main.py` in the `YAML-Python` repl: `file_to_parse`
1. Run the Repl, troubleshoot any issues, and examine the output.

**NOTE**: You will be using this file in upcoming labs.  If you're having trouble, ask a classmate or teacher!

**NOTE**: You don't need to do all the labs in both Bash and Python.  You can pick one or the other; whichever you're more comfortable with.

**ANOTHER NOTE**: If you're trying to use the shell in `YAML-Bash` and getting an error about the `yq` command, just click the `Run` button.  The `main.sh` script will install a newer version than is available through Replit.com.