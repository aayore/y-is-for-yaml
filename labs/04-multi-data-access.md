# More Complex Data Access

---

## Organizing Data

In most cases, your YAML structure will be dictated by a tool or application.  But when you're creating your own data structure, you need to think about how it will be accessed.  In our `doggy.yaml` example, we're storing data in a key.  In this case, `"Fido"`.  This can make it difficult to access.  We can't reference the name of the dog using metadata, which can be a problem.  (If our data collection were *only* going to be about Fido, this might be fine.  But if we want to add other animals, we should give our collection a more cogent name.)

Let's change our structure and add a friend for Fido:

```yaml
Pets:
    -   Name: Fido
        Species: Dog
        Age: 4
        YOB: "2018"
        Male: true
        WeightKg: 36.23
        VetVisits:
            "2022-06-18":
                - Bordetella
            "2022-03-09":
                - Rabies
                - Distemper
    -   Name: Barkley
        Species: Muppet
        Age: 43
        YOB: "1978"
        Male: true
        WeightKg: 12.7

```

Shell:
```shell
cat doggy.yaml | yq '.Pets[].Name'

```

NOTE: The `yq` syntax for accessing a Block Sequence - `.key[]` or `.[]` - doesn't require any language that implies looping (`for`, `while`, etc.)  The looping happens automatically and we don't have to worry about it.

Python:
```python
## Remember how we load the data:
# import yaml,datetime
#
# with open('doggy.yaml','r') as file:
#   yaml_string = ''.join(file)
#
# data = yaml.safe_load(yaml_string)

for pet in data['Pets']:
    print(pet['Name'])

```

Alternatively, we can omit the primary key:

```yaml
-   Name: Fido
    Species: Dog
    Age: 4
    YOB: "2018"
    Male: true
    WeightKg: 36.23
    VetVisits:
        "2022-06-18":
            - Bordetella
        "2022-03-09":
            - Rabies
            - Distemper
-   Name: Barkley
    Species: Muppet
    Age: 43
    YOB: "1978"
    Male: true
    WeightKg: 12.7

```


Shell:
```shell
cat doggy.yaml | yq '.[] | .Name'
# or
cat doggy.yaml | yq '.[].Name'
```

Python:
```python
for pet in data:
    print(pet['Name'])

# or

print('\n'.join(pet['Name'] for pet in data))
```

---

## Things to think about

How will your data evolve?

The following are both perfectly valid structures, but one may be better-suited to your data and use case.

In this case, your biggest container will be a Mapping:
```yaml
Dogs:
    -   Name: Fido
        ...
    -   Name: Barkley
        ...
Cats:
    -   Name: Felix
        ...
    -   Name: Garfield
```

In this case, your biggest container will be a Block Sequence:
```yaml
-   Name: Fido
    Species: Dog
    ...
-   Name: Barkley
    Species: Dog
    ...
-   Name: Felix
    Species: Cat
    ...
-   Name: Garfield
    Species: Cat
    ...
```

---

## Our Data

For the rest of this lesson, we are going to omit the primary data key and use a Block Sequence to store our data:

```yaml
-   Name: Fido
    Species: Dog
    Age: 4
    YOB: "2018"
    Male: true
    WeightKg: 36.23
    VetVisits:
        "2022-06-18":
            - Bordetella
        "2022-03-09":
            - Rabies
            - Distemper
-   Name: Barkley
    Species: Muppet
    Age: 43
    YOB: "1978"
    Male: true
    WeightKg: 12.7
```

---

## Accessing Multiple Elements

Imagine we want to view only a subset of the data: **name** and **age**

**Bash**

```shell
cat doggy.yaml | yq '.[] | {.Name: .Age}'
```

There's a lot going on in this example.  We're going to focus on the `yq` instructions: `'.[] | {.Name: .Age}'`

- `.[]` tells `yq` to operate on the Block Sequence at the root (`.`) of our data object.
- `|` tells `yq` to pass the output of the first operation (`.[]`) to the second operation (`{.Name: .Age}`).
- The `{key: value}` syntax is a constructor - we're creating a new Mapping object.
- We're using `.Name` for the key and `.Age` for the value.

Because `yq` handles the looping, you can think of `.my_key[] | some_operation` like:

```shell
for each_element in my_key; do
    some_operation(each_element)
done
```

All said and done, we should have output that looks like this:

```yaml
Fido: 4
Barkley: 43
```

If you wanted a Block Sequence of Mappings, you could wrap the whole thing in a Block Sequence constructor:

```shell
cat doggy.yaml | yq '[.[] | {.Name: .Age}]'
```

To get this:

```yaml
- Fido: 4
- Barkley: 43
```

**Python**

Python is perhaps more legible, using just a [for loop](https://docs.python.org/3/reference/compound_stmts.html#the-for-statement) and [f-strings](https://docs.python.org/3/reference/lexical_analysis.html#f-strings).

```python
# import yaml,datetime
# with open('doggy.yaml','r') as file:
#   yaml_string = ''.join(file)

# data = yaml.safe_load(yaml_string)

for pet in data:
    print(f"{pet['Name']}: {pet['Age']}")

```

Alternatively, if you wanted this as a new data structure (instead of printed to the screen):

```python
name_age_list = [ {pet['Name']: pet['Age']} for pet in data ]
```

Results in Python data like this:

```python
[{'Fido': 4}, {'Barkley': 43}]
```

---

## Selecting Data

**Bash**

`yq` provides a number of handy functions.  `select()` allows you to easily pick and choose which elements you want.

```shell
cat doggy.yaml | yq '[.[] | select(.Age > 10)]'
```

**Python**

There are multiple ways to do this in Python:

```python
# Using a generator:
select_data = [ dog for dog in data if dog['Age'] > 10 ]

# Using the built-in filter() function:
select_data = list( filter( lambda d: d['Age'] > 10, data ) )

# Brute force looping:
select_data = []
for dog in data:
    if dog['Age'] > 10:
        select_data.append( dog )
```

---

## Lab

Did your lightbulb just go on?  Your boss asked you for a list of game titles and their release dates.  Now you know a better way to deliver this: a single command instead of two!  Get to it!

Also, your VP of Partnerships has asked you to provide a list of games that will be on the Xbox platform.

1. Reorganize your `games.yaml` to omit the primary key, e.g.:
    ```yaml
    -   Title: your_first_title
        ...
    -   Title: your_second_title
        ...
    ```
1. Generate a list of `Title: Release Date` data for your boss using a single command.
1. Use a single command to select only games that will be available on the Xbox platform.

*Extra Credit:*

- Can you come up with an efficient way to filter/sort/group the data by platform?

```yaml
Xbox:
    -   your_second_title
Switch:
    -   your_first_title
    -   your_second_title
```

If it helps, your instructor was trying (and failing) to do it with something like this:

```shell
cat games.yaml | yq \
'.[] | {
    "Xbox": [
        select( contains( {"List of release platforms": ["Xbox"]} ))
    ],
    "Switch": [
        select( contains( {"List of release platforms": ["Switch"]} ))
    ]
}'
```
