# Accessing YAML Elements

---

Because YAML is essentially a human-readable data format, it's important to know how to access the data.  We're going to access elements using both Python and [yq](https://mikefarah.gitbook.io/yq/) (a bash `y`aml `q`uery tool - though it's really just a wrapper around [jq](https://stedolan.github.io/jq/)).  But first, we're going to look at a common human-readable notation.

We're going to use the example from the last lesson:

```yaml
Fido:
    Species: Dog
    Age: 4
    YOB: "2018"
    Male: true
    WeightKg: 36.23
    VetVisits:
        2022-06-18:
            - Bordetella
        2022-03-09:
            - Rabies
            - Distemper
```

---

## The Human-Readable Way

You will sometimes see data elements referred to like this:

`.Fido.WeightKg`

This is handy because we read the tree: We look at `Fido`, then at `WeightKg`, and we know that `36.23` is the value we're after.

This can get a little tricky with Block Sequences:

`.Fido.VetVisits.2022-03-09[1]`

Most tools - YAML parsers, in particular - use zero-indexing.  This means the first element of a Block Sequence will usually be [0].  In this case, the value we're looking for is `Distemper`.  To look for `Rabies`, we would use `.Fido.VetVisits.2022-03-09[0]`.

---

## Example File

For the next few examples, we're going to save the YAML block above in a file called `doggy.yaml`.

---

## The Python Way

If you're familiar with Python, loading the YAML is the only tricky part.  Accessing the data will be the easiest thing you do today.

We load our `doggy.yaml` data into Python like this:

```python
import yaml,datetime

with open('doggy.yaml','r') as file:
  yaml_string = ''.join(file)

data = yaml.safe_load(yaml_string)
```

**Note**: `yaml.safe_load()` returns a single object (string).  This will barf if there's a document separator (`---`) in your YAML file.  To handle multiple documents in a single file, you must use `yaml.safe_load_all()`, which returns a Python generator (which can easily be converted to a list: `list(yaml.safe_load_all())` ).  Then you have to treat your data as a list of strings instead of a single string.

Now to access the data:

```python
data['Fido']['WeightKg']
```

If this looks familiar, it's becuase the YAML data has been loaded into Python as a [dictionary](https://docs.python.org/3/tutorial/datastructures.html#dictionaries).

A YAML Block Sequence is essentially a [list](https://docs.python.org/3/tutorial/introduction.html#lists) in Python.  So we can access our specific vaccination data like this:

```python
data['Fido']['VetVisits'][datetime.date(2022, 3, 9)][1]
```

Maybe "easiest thing you do today" was an overstatement.  Because - like YAML - Python dictionary keys can be objects other than strings, we have to `import datetime` and use a `datetime.date()` object to access the data we want.

In a Python console, it will look like this:

```python
>>> data['Fido']['WeightKg']
36.23
>>> data['Fido']['VetVisits'][datetime.date(2022, 3, 9)][1]
'Distemper'
```

---

## A Bash Way

There are tons of tools in Bash that could do this job in some fashion or another.  Some combination of `sed`, `awk`, `grep`, `cut`, etc. would work.  There's at least one person in this class who wants to play Perl golf on the command line and pass it off as Bash.  But there's a purpose-built tool for this job, and it makes things easy.

[yq](https://mikefarah.gitbook.io/yq/) - **Y**AML **Q**uery - is the YAML equivalent to the very popular [jq](https://stedolan.github.io/jq/).  (It should already be set up and ready to go in your `YAML-Bash` Repl.)

**Note**: Even though it's valid YAML, `yq` doesn't like dates as Mapping keys.  (Maybe there's a lesson in this somewhere.)  We have to modify the file slightly:

```yaml
Fido:
    Species: Dog
    Age: 4
    YOB: "2018"
    Male: true
    WeightKg: 36.23
    VetVisits:
        "2022-06-18":
            - Bordetella
        "2022-03-09":
            - Rabies
            - Distemper
```

To access the same data as above:

```shell
yq '.Fido.WeightKg' doggy.yaml
```

There's a whole thing about [UUOC](https://porkmail.org/era/unix/award) (Useless Use of Cat).  But I prefer the following syntax - especially when iterating on a query to find what I want.

```shell
cat doggy.yaml | yq '.Fido.WeightKg'
```

Having the query at the end of the line makes it easier to edit (in my opinion).

Similarly, to access our second bit of example data:

```shell
cat doggy.yaml | yq '.Fido.VetVisits."2022-03-09"[1]'
```

---

## Lab

Your boss wants a list of game titles and their release dates.  Knowing what you know about YAML, this should be pretty easy.  Using the file you created in the last lab...

1. Run a command to list the game titles.
1. Run a command to list the release dates.

**HINT**: In `yq`, you must put both quotes and square brackets around any key that includes a special character (in this case a space).  

```shell
echo My Key: My Value | yq '.["My Key"]'
```
