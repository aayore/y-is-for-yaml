
## Writing YAML

It is possible to construct a YAML document using `yq`

You can start with `echo "" | yq ...` to give `yq` an empty string on which to operate.  Or you can just use the `-n, --null-input` option built in to `yq`:

```shell
yq -n '.numbers = [1, 2, 3], .letters = ["a", "b", "c"]' > myfile.yaml
cat myfile.yaml
```
Will result in:

```yaml
numbers:
  - 1
  - 2
  - 3
letters:
  - a
  - b
  - c

```

Similarly, any (AFAIK) Python data object can be exported as YAML:

```python
import yaml
data = {'numbers':[1,2,3],'letters':['a','b','c']}
print(yaml.dump(data))
```

Will result in:

```yaml
letters:
- a
- b
- c
numbers:
- 1
- 2
- 3

```

This example demonstrates:

- The order of objects in a Mapping type does not matter.  You should not expect it to be preserved.
- Note that in both cases, `numbers` came first in the definition.  This was not consistent in the output.

Which leads in to:

- The order of objects in a Block Sequence do matter.  It will be preserved.

---

## All JSON is YAML.  Not all YAML is JSON.

You can test the assertion from Lab 02 that JSON is YAML:

```shell
yq -n '{ "Dogs": [ "Fido", "Barkley" ], "Caretaker": "Big Bird" }'
```

Results in:

```yaml
Dogs:
  - Fido
  - Barkley
Caretaker: Big Bird
```

---

## Modifying YAML with `yq`

Start with the same file we generated above:

```shell
yq -n '.numbers = [1, 2, 3], .letters = ["a", "b", "c"]' > myfile.yaml
```

We can use the `+=` (relative append) operator to modify our YAML:

```shell
cat myfile.yaml | yq '.numbers += 4 | .letters += "d"'
```

See what happens if we use `+` (append) instead of `+=` (relative append):

```shell
cat myfile.yaml | yq '.numbers + 4'
```

Because this is not a relative operation, it removes the `.letters` block.

To edit a file in place, you can use the `-i, --inplace` option for `yq`:

```shell
yq --inplace '.words = ["hello", "world"]' myfile.yaml
cat myfile.yaml
```

You can see the new object in the file:

```yaml
numbers:
  - 1
  - 2
  - 3
letters:
  - a
  - b
  - c
words:
  - hello
  - world
```