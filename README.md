# Y is for YAML

Originally `Yet Another Markup Language`, [YAML](https://yaml.org/) had already [pivoted](https://www.youtube.com/watch?v=M1vfXoUNDYA) to `YAML Ain't Markup Language` prior to the [1.0 spec](https://yaml.org/spec/1.0/) released in early 2004.  Traditional markup languages like HTML, XML, and Markdown focus on inline document processing or notation.  But YAML - like [JSON](https://www.json.org/json-en.html) - is really a [data-serialization language](https://en.wikipedia.org/wiki/Serialization) that represents data structures in a mostly human-reabale format.  You can think of it like a human-computer interface for representing data.

---

### Class Goals

1. Understand the three main object types:
    1. Block sequences (a.k.a. list or array)
    1. Mappings (a.k.a. dictionary or hash)
    1. Comments (a.k.a. not allowed in JSON)
1. Access YAML data programmatically
1. Minimize repetition with anchors and aliases
1. Work with strings in YAML

---

### Prerequisites:

- Web browser
- Your own account at [Replit](https://replit.com/)

---

### Labs

1. [Getting Set Up](labs/01-setup.md)
1. [Writing Basic YAML](labs/02-writing-basic-yaml.md)
1. [Accessing YAML Elements](labs/03-accessing-yaml-elements.md)
1. [Accessing More Complex Data](labs/04-multi-data-access.md)
1. [Using Anchors and Aliases](labs/05-anchors-and-repetition.md)
1. [Working With Strings](labs/06-working-with-strings.md)

---

### Reference and Recommended Reading
- [Official YAML Reference Card](https://yaml.org/refcard.html)
- [YAML on Wikipedia](https://en.wikipedia.org/wiki/YAML)
- [Learn X in Y Minutes](https://learnxinyminutes.com/docs/yaml/)
